﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

using Argotic.Syndication;

namespace fortrss
{
    public class Feed : INotifyPropertyChanged
    {
        public Feed()
        {
            underlyingFeed = null;
            feedUri = null;
            Title = "<Empty title>";
        }

        public Feed(GenericSyndicationFeed underlyingFeed, Uri feedUri)
        {
            this.underlyingFeed = underlyingFeed;
            this.feedUri = feedUri;
            this.Title = underlyingFeed.Title;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
        }

        public string FeedUri
        {
            get
            {
                return feedUri.ToString();
            }
            set
            {
                feedUri = new Uri(value);
            }
        }

        public string Title { get; set; }

        [XmlIgnore]
        public GenericSyndicationFeed UnderlyingFeed
        {
            get
            {
                return underlyingFeed;
            }
            set 
            {
                underlyingFeed = value;
            }
        }

        private GenericSyndicationFeed underlyingFeed;
        private Uri feedUri;
    }
}
