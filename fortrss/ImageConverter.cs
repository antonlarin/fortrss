﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace fortrss
{
    public class ImageConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType,
                                       object parameter, CultureInfo culture)
        {
            if (value != null && (value as string != ""))
            {
                BitmapImage image = new BitmapImage();
                image.BeginInit();
                image.UriSource = new Uri(value.ToString(), UriKind.Absolute);
                image.EndInit();

                return image;
            }
            else
            {
                return null;
            }
        }

        object IValueConverter.ConvertBack(object value, Type targetType,
                                           object parameter,
                                           CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
