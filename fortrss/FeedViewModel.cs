﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Runtime.Remoting.Messaging;
using System.Reflection;

using System.Windows;
using System.Windows.Threading;
using System.Windows.Input;
using System.Windows.Controls;

using System.IO;

using System.ComponentModel;
using System.Web;
using System.Net;

using System.Xml;
using System.Xml.Serialization;

using Argotic.Syndication;
using Argotic.Common;
using Argotic.Extensions.Core;

namespace fortrss
{
    enum DisplayType
    {
        ShowingRegularFeed, ShowingFavoriteItems,
        RetrievingFeed
    };

    public class ReaderViewModel : INotifyPropertyChanged
    {
        public ReaderViewModel()
        {
            ViewFeedCommand = new DelegateCommand(ViewFeed);
            DiscardNewSettingsCommand =
                new DelegateCommand(DiscardNewSettings);
            BookmarkCurrentFeedCommand =
                new DelegateCommand(BookmarkCurrentFeed);
            ViewFavoriteItemsCommand =
                new DelegateCommand(ViewFavoriteItems);

            CurrentFeedItems = new ObservableCollection<FeedItem>();

            try
            {
                BookmarkedFeeds = LoadBookmarkedFeeds();
            }
            catch (FileNotFoundException)
            {
                BookmarkedFeeds = new ObservableCollection<Feed>();
            }

            try
            {
                FavoriteFeedItems = LoadFavoriteFeedItems();
            }
            catch (FileNotFoundException)
            {
                FavoriteFeedItems = new ObservableCollection<FeedItem>();
            }

            try
            {
                Settings = new ReaderSettings("settings.xml");
            }
            catch (FileNotFoundException)
            {
                Settings = new ReaderSettings();
            }

            SelectedBookmarkedFeedIndex = -1;
            ViewFavoriteItems();
        }

        #region Notification services

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
        }

        #endregion


        #region Commands and properties for bindings in views

        public ICommand ViewFeedCommand { get; set; }
        public ICommand BookmarkCurrentFeedCommand { get; set; }
        public ICommand OpenSettingsDialogCommand { get; set; }
        public ICommand DiscardNewSettingsCommand { get; set; }
        public ICommand ViewFavoriteItemsCommand { get; set; }

        public ObservableCollection<FeedItem> CurrentFeedItems { get; set; }
        public ObservableCollection<FeedItem> FavoriteFeedItems { get; set; }
        public ObservableCollection<Feed> BookmarkedFeeds { get; set; }

        public string FeedUriString
        {
            get { return feedUriString; }
            set
            {
                if (!value.StartsWith("http://") &&
                    !value.StartsWith("https://"))
                    feedUriString = "http://" + value;
                else
                    feedUriString = value;
            }
        }
        public ReaderSettings Settings { get; set; }
        public ReaderSettings OldSettings { get; set; }
        public int SelectedBookmarkedFeedIndex { get; set; }
        public bool BookmarkedExistingFeed { get; set; }

        public Visibility RetrievingFeed
        {
            get
            {
                return (displayType == DisplayType.RetrievingFeed) ?
                    Visibility.Visible : Visibility.Collapsed;
            }
        }

        public Visibility ShowingFeed
        {
            get
            {
                return (displayType == DisplayType.ShowingRegularFeed ||
                        displayType == DisplayType.ShowingFavoriteItems) ?
                    Visibility.Visible : Visibility.Collapsed;
            }
        }

        public Visibility ShowingRegularFeed
        {
            get
            {
                return (displayType == DisplayType.ShowingRegularFeed) ?
                    Visibility.Visible : Visibility.Collapsed;
            }
        }

        public Visibility ShowingFavoriteItems
        {
            get
            {
                return (displayType == DisplayType.ShowingFavoriteItems) ?
                    Visibility.Visible : Visibility.Collapsed;
            }
        }

        #endregion


        #region Implementation for binding elements

        private void ViewFeed()
        {
            displayType = DisplayType.RetrievingFeed;
            NotifyScreenChanged();

            AsyncFeedDownloader downloader =
                new AsyncFeedDownloader(DownloadFeed);
            downloader.BeginInvoke(feedUriString,
                new AsyncCallback(FinishNewFeedDownload), null);
        }

        private void BookmarkCurrentFeed()
        {
            if (currentFeed == null)
                return;

            foreach (var feed in BookmarkedFeeds)
                if (feed.FeedUri == currentFeed.FeedUri)
                {
                    BookmarkedExistingFeed = true;
                    NotifyPropertyChanged("BookmarkedExistingFeed");
                    return;
                }

            BookmarkedFeeds.Add(currentFeed);
            NotifyPropertyChanged("BookmarkedFeeds");
        }

        private void DiscardNewSettings()
        {
            Settings = OldSettings;
            OldSettings = null;
        }

        private void ViewFavoriteItems()
        {
            displayType = DisplayType.ShowingFavoriteItems;
            CurrentFeedItems = FavoriteFeedItems;
            NotifyScreenChanged();
            NotifyPropertyChanged("CurrentFeedItems");
        }

        private string feedUriString;
        private Feed currentFeed;
        private DisplayType displayType;

        #endregion


        #region Other methods

        public void DeleteBookmarkedFeed(Feed feed)
        {
            BookmarkedFeeds.Remove(feed);
            NotifyPropertyChanged("BookmarkedFeeds");
        }

        public void AddFeedItemToFavorites(FeedItem item)
        {
            FavoriteFeedItems.Add(item);
        }

        public void RemoveFeedItemFromFavorites(FeedItem item)
        {
            FavoriteFeedItems.Remove(item);
            NotifyPropertyChanged("FavoriteFeedItems");
        }

        public void SaveResources()
        {
            Settings.WriteSettings("settings.xml");
            WriteBookmarkedFeeds();
            WriteFavoriteFeedItems();
        }

        public void DisplaySelectedBookmarkedFeed()
        {
            int index = SelectedBookmarkedFeedIndex;
            currentFeed = BookmarkedFeeds[index];

            displayType = DisplayType.RetrievingFeed;
            NotifyScreenChanged();

            if (currentFeed.UnderlyingFeed == null)
            {
                AsyncFeedDownloader downloader =
                    new AsyncFeedDownloader(DownloadFeed);
                downloader.BeginInvoke(currentFeed.FeedUri,
                    FinishBookmarkedFeedDownload, null);
            }
            else
            {
                DisplayCurrentFeed(); 
            }
        }

        public void FinishNewFeedDownload(IAsyncResult asyncresult)
        {
            AsyncResult result = (AsyncResult)asyncresult;
            AsyncFeedDownloader downloader =
                (AsyncFeedDownloader)result.AsyncDelegate;

            currentFeed = downloader.EndInvoke(asyncresult);

            App.Current.Dispatcher.Invoke((Action)delegate
                {
                    DisplayCurrentFeed();
                });
        }

        public void FinishBookmarkedFeedDownload(IAsyncResult asyncresult)
        {
            AsyncResult result = (AsyncResult)asyncresult;
            AsyncFeedDownloader downloader =
                (AsyncFeedDownloader)result.AsyncDelegate;

            currentFeed = downloader.EndInvoke(asyncresult);

            App.Current.Dispatcher.Invoke((Action)delegate
                {
                    BookmarkedFeeds[SelectedBookmarkedFeedIndex] = currentFeed;
                    DisplayCurrentFeed();
                });
        }

        private Feed DownloadFeed(string feedUriString)
        {
            Feed targetFeed = null;
            try
            {
                if (Settings.UsingProxy)
                {
                    int port = Int32.Parse(Settings.ProxyPort);
                    Uri proxyUri = new UriBuilder("http", Settings.ProxyAddress,
                                              port).Uri;
                    WebProxy proxy = new WebProxy(proxyUri);
                    Uri feedUri = new Uri(feedUriString);
                    targetFeed = new Feed(
                        GenericSyndicationFeed.Create(feedUri, null, proxy),
                        feedUri);
                }
                else
                {
                    Uri feedUri = new Uri(feedUriString);
                    targetFeed = new Feed(
                        GenericSyndicationFeed.Create(feedUri),
                        feedUri);
                }
            }
            catch (WebException e)
            {
                MessageBox.Show(e.Message, "Connection error",
                                MessageBoxButton.OK,
                                MessageBoxImage.Exclamation);
            }
            catch (XmlException)
            {
                MessageBox.Show("Error during feed processing.",
                                "Feed processing error",
                                MessageBoxButton.OK,
                                MessageBoxImage.Exclamation);
            }

            return targetFeed;
        }

        private delegate Feed AsyncFeedDownloader(string feedUriString);

        private void DisplayCurrentFeed()
        {
            CurrentFeedItems = new ObservableCollection<FeedItem>();
            if (currentFeed.UnderlyingFeed.Format ==
                    SyndicationContentFormat.Rss)
            {
                RssFeed rss = currentFeed.UnderlyingFeed.Resource as RssFeed;
                if (rss != null)
                {
                    foreach (var item in rss.Channel.Items)
                    {
                        string link = item.Link.AbsoluteUri;
                        CurrentFeedItems.Add(new FeedItem(item.Title,
                            HttpUtility.HtmlDecode(item.Description), link));
                    }

                }
            }
            else if (currentFeed.UnderlyingFeed.Format ==
                SyndicationContentFormat.Atom)
            {
                AtomFeed atom =
                    currentFeed.UnderlyingFeed.Resource as AtomFeed;
                if (atom != null)
                {
                    foreach (var item in atom.Entries)
                    {
                        string link = item.Links[0].Uri.AbsoluteUri;

                        CurrentFeedItems.Add(new FeedItem(item.Title.Content,
                            HttpUtility.HtmlDecode(item.Content.Content), link));
                    }
                }
            }

            NotifyPropertyChanged("CurrentFeedItems");

            displayType = DisplayType.ShowingRegularFeed;
            NotifyScreenChanged();
        }

        private void NotifyScreenChanged()
        {
            NotifyPropertyChanged("WelcomeScreen");
            NotifyPropertyChanged("ShowingRegularFeed");
            NotifyPropertyChanged("RetrievingFeed");
            NotifyPropertyChanged("ShowingFavoriteItems");
            NotifyPropertyChanged("ShowingFeed");
        }

        private ObservableCollection<Feed> LoadBookmarkedFeeds()
        {
            TextReader bookmarkedFeedsReader =
                new StreamReader("bookmarkedfeeds.xml");
            XmlSerializer serializer =
                new XmlSerializer(typeof(ObservableCollection<Feed>));
            ObservableCollection<Feed> result =
                (ObservableCollection<Feed>)serializer.
                Deserialize(bookmarkedFeedsReader);
            bookmarkedFeedsReader.Close();
            return result;
        }

        private void WriteBookmarkedFeeds()
        {
            TextWriter bookmarkedFeedsWriter =
                new StreamWriter("bookmarkedfeeds.xml", false);
            XmlSerializer serializer =
                new XmlSerializer(typeof(ObservableCollection<Feed>));
            serializer.Serialize(bookmarkedFeedsWriter, BookmarkedFeeds);
            bookmarkedFeedsWriter.Close();
        }

        private ObservableCollection<FeedItem> LoadFavoriteFeedItems()
        {
            TextReader favoriteFeedItemsReader =
                new StreamReader("favoritefeeditems.xml");
            XmlSerializer serializer =
                new XmlSerializer(typeof(ObservableCollection<FeedItem>));
            ObservableCollection<FeedItem> result =
                (ObservableCollection<FeedItem>)serializer.
                Deserialize(favoriteFeedItemsReader);
            favoriteFeedItemsReader.Close();
            return result;
        }

        private void WriteFavoriteFeedItems()
        {
            TextWriter favoriteFeedItemsWriter =
                new StreamWriter("favoritefeeditems.xml");
            XmlSerializer serializer =
                new XmlSerializer(typeof(ObservableCollection<FeedItem>));
            serializer.Serialize(favoriteFeedItemsWriter, FavoriteFeedItems);
            favoriteFeedItemsWriter.Close();
        }

        #endregion
    }
}