﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace fortrss
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = App.Model;
        }

        private void SettingsButtonClick(object sender, RoutedEventArgs e)
        {
            App.Model.OldSettings = (ReaderSettings)App.Model.Settings.Clone();

            SettingsWindow dialog = new SettingsWindow();
            dialog.Owner = this;
            dialog.ShowDialog();
        }

        private void BookmarkedFeedsListSelectionChanged(object sender,
            SelectionChangedEventArgs e)
        {
            if (BookmarkedFeedsList.SelectedIndex > -1)
            {
                App.Model.DisplaySelectedBookmarkedFeed();
            }
        }

        private void DeleteButtonClick(object sender, RoutedEventArgs e)
        {
            Button deleteButton = sender as Button;
            App.Model.DeleteBookmarkedFeed(deleteButton.DataContext as Feed);
        }

        private void AddFeedItemToFavorites(object sender, RoutedEventArgs e)
        {
            Button favoriteButton = sender as Button;
            App.Model.AddFeedItemToFavorites(favoriteButton.DataContext as FeedItem);
        }

        private void RemoveFeedItemFromFavorites(object sender, RoutedEventArgs e)
        {
            Button removeButton = sender as Button;
            App.Model.RemoveFeedItemFromFavorites(removeButton.DataContext as FeedItem);
        }
    }
}
