﻿using System.Windows;

namespace fortrss
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {
        public SettingsWindow()
        {
            InitializeComponent();
            DataContext = App.Model;
        }

        private void AcceptAndCloseWindow(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void DiscardAndCloseWindow(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
