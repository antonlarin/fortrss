﻿using System;
using System.Windows.Input;

namespace fortrss
{
    class DelegateCommand: ICommand
    {
        public event EventHandler CanExecuteChanged;

        public DelegateCommand(Action action)
        {
            mAction = action;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            mAction();
        }

        private readonly Action mAction;
    }
}
