﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Diagnostics;
using System.Net;
using System.Runtime.Remoting.Messaging;
using System.ComponentModel;
using System.IO;
using System.Xml.Serialization;

using System.Text.RegularExpressions;

namespace fortrss
{
    public class FeedItem
    {
        public FeedItem()
        {
            Title = "";
            Summary = "";
            FullArticleLink = "";
            ItemImageLink = null;
        }

        public FeedItem(string title, string summary,
            string link)
        {
            Title = title;
            Summary = RemoveExcessiveWhitespace(RemoveHtml(summary));
            FullArticleLink = link;
            string imageLink = ExtractImageLink(summary);
            if (imageLink.Equals(""))
            {
                ItemImageLink = null;
            }
            else
            {
                ItemImageLink = imageLink;
            }
            
            GoToFullArticleCommand = new DelegateCommand(GoToFullArticle);
        }

        [XmlIgnore]
        public ICommand GoToFullArticleCommand { get; set; }

        public void GoToFullArticle()
        {
            Process.Start(FullArticleLink);
        }

        public string Title { get; set; }
        public string ItemImageLink { get; set; }
        public string Summary { get; set; }
        public string FullArticleLink { get; set; }

        private string RemoveHtml(string input)
        {
            return Regex.Replace(input, "<[^>]+>", "", RegexOptions.Compiled);
        }

        private string RemoveExcessiveWhitespace(string input)
        {
            string result = input;
            string old = null;
            do
            {
                old = result;
                result = old.Replace("\n\n", "\n");
            }
            while (old != result);
            return result;
        }

        private string ExtractImageLink(string input)
        {
            Match imgTag = Regex.Match(input,
                @"<\s*img[^>]*src\s*=\s*""(?<link>[^>""]*)""[^>]*>",
                RegexOptions.Compiled);
            string link = imgTag.Groups["link"].Value;
            return link;
        }
    }
}
