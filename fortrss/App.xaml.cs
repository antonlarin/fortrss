﻿using System.Windows;

namespace fortrss
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            Model = new ReaderViewModel();
        }

        public static ReaderViewModel Model { get; set; }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            Model.SaveResources();
        }
    }
}
