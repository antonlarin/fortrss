﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace fortrss
{
    public class ReaderSettings : INotifyPropertyChanged, ICloneable
    {
        public ReaderSettings()
        {
            UsingProxy = false;
            ProxyAddress = "";
            ProxyPort = "";
        }

        public ReaderSettings(bool usingproxy, string proxyaddress,
            string proxyport)
        {
            UsingProxy = usingproxy;
            ProxyAddress = proxyaddress;
            ProxyPort = proxyport;
        }

        public ReaderSettings(string path)
        {
            ReadSettings(path);
        }

        public void ReadSettings(string path)
        {
            TextReader fileReader = new StreamReader(path);
            XmlSerializer serializer = new XmlSerializer(typeof(ReaderSettings));
            ReaderSettings settings = (ReaderSettings)serializer.Deserialize(fileReader);

            this.UsingProxy = settings.UsingProxy;
            this.ProxyAddress = settings.ProxyAddress;
            this.ProxyPort = settings.ProxyPort;

            fileReader.Close();
        }

        public void WriteSettings(string path)
        {
            TextWriter fileWriter = new StreamWriter(path, false);
            XmlSerializer serializer = new XmlSerializer(typeof(ReaderSettings));
            serializer.Serialize(fileWriter, this);
            fileWriter.Close();
        }

        public object Clone()
        {
            return new ReaderSettings(UsingProxy, ProxyAddress, ProxyPort);
        }
        
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
        }

        public bool UsingProxy { get; set; }
        public string ProxyAddress { get; set; }
        public string ProxyPort { get; set; }
    }
}
;
